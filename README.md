# e-paper

## Übersicht

- D1 Mini Wemos/AZDelivery ESP8266-12F
- Waveshare 1.54inch e-Paper Module

### Links
- https://www.waveshare.com/product/1.54inch-e-paper-module-b.htm
- https://www.exp-tech.de/displays/e-paper-e-ink/8286/200x200-1.54-e-ink-display-module-three-color?c=1424

- https://www.az-delivery.de/en/blogs/azdelivery-blog-fur-arduino-und-raspberry-pi/e-paper-display-am-esp32-und-esp8266-teil1
- https://www.makerblog.at/2020/10/waveshare-29-e-paper-display-am-arduino-uno/

- https://github.com/soonuse/epd-library-arduino
- https://github.com/ZinggJM/GxEPD2


## Pinout
### Display
- BUSY -> D2
- RST  -> ~~D4~~ D1
- DC   -> D3
- CS   -> D8
- CLK  -> D5
- DIN  -> D7
- GND  -> GND
- 3.3V -> 3.3V
### LED
- 5V -> 5V
- DIN -> D6
- GND -> GND

## upload firmware
paper\_test, paper\_test_2, paper\_test\_3
```
make upload
```

## upload images
paper\_test\_3
```
make spiffs
```

## convert images
```
wget --content-disposition https://img.playbuzz.com/image/upload/ar_1.0,c_pad,f_jpg,b_auto/q_auto:good,f_auto,fl_lossy,w_640,c_limit/cdn/9576ca64-ec47-4b7e-808f-5440446e4137/b18d4e24-456c-4460-86b4-5e87ae02b1b1.jpg -o klausi_download.jpeg
```
```
convert -resize 200x200 klausi_download.jpeg -monochrome  klausi_bw.bmp
```
https://stackoverflow.com/a/56095736
```
convert xc:red xc:white xc:black +append palette.gif
convert -resize 200x200 klausi_download.jpeg -remap palette.gif klausi_3c.bmp
```
## saleae Logic 2 Aufnahme
Saleae Aufnahme von paper\_test: siehe [saleae capture](saleae/ePaper.sal).  
Aufgenommen mit saleae Logic Pro 16 und Software [saleae alpha 2.3.20](https://ideas.saleae.com/f/changelog/).
![](saleae/saleae_screenshot.png)

# word clock adafruit 8x8 neomatrix
-based on
https://github.com/andydoro/WordClock-NeoMatrix8x8
- logic for german 'words'from
https://github.com/robotfreak/WordClock_Rainbow_de
https://github.com/formatc1702/Micro-Word-Clock/tree/master/MicroWordClock2-Arduino
-ntp
https://platformio.org/lib/show/551/NTPClient/examples