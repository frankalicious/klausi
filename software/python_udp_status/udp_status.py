#!/usr/bin/env python3

import socket
import time

UDP_IP = "192.168.0.48"
UDP_PORT = 5555
messages = ["1:init"]
#messages = []
for i in range(1,11):
    messages.append("{}:0xcc00cc".format(i))
    messages.append("{}:0x0".format(i))

print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)
print("message:", messages)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
for message in messages:
    sock.sendto(bytes(message, "utf-8"), (UDP_IP, UDP_PORT))
    time.sleep(0.5)