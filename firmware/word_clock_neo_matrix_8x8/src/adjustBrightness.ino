
// change brightness based on the time of day.

void adjustBrightness(int hours) {

  // get time from the RTC
  //DateTime thetime = RTC.now();
  
  //change brightness if it's night time
  if (hours < MORNINGCUTOFF || hours > NIGHTCUTOFF) {
    matrix.setBrightness(NIGHTBRIGHTNESS);
  } else {
    matrix.setBrightness(DAYBRIGHTNESS);
  }
}
