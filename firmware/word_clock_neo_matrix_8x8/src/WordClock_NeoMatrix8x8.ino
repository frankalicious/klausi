/*
   WORD CLOCK - 8x8 NeoPixel Desktop Edition
   by Andy Doro

   A word clock using NeoPixel RGB LEDs for a color shift effect.

   Hardware:
   - Trinket Pro 5V (should work with other Arduino-compatibles with minor modifications)
   - DS1307 RTC breakout
   - NeoPixel NeoMatrix 8x8


   Software:

   This code requires the following libraries:

   - RTClib https://github.com/adafruit/RTClib
   - DST_RTC https://github.com/andydoro/DST_RTC
   - Adafruit_GFX https://github.com/adafruit/Adafruit-GFX-Library
   - Adafruit_NeoPixel https://github.com/adafruit/Adafruit_NeoPixel
   - Adafruit_NeoMatrix https://github.com/adafruit/Adafruit_NeoMatrix


   Wiring:
   - Solder DS1307 breakout to Trinket Pro, A2 to GND, A3 to PWR, A4 to SDA, A5 to SCL
   If you leave off / clip the unused SQW pin on the RTC breakout, the breakout can sit right on top of the Trinket Pro
   for a compact design! It'll be difficult to reach the Trinket Pro reset button, but you can activate the bootloader by
   plugging in the USB.
   - Solder NeoMatrix 5V to Trinket 5V, GND to GND, DIN to Trinket Pro pin 8.


   grid pattern

    A T W E N T Y D
    Q U A R T E R Y
    F I V E H A L F
    D P A S T O R O
    F I V E I G H T
    S I X T H R E E
    T W E L E V E N
    F O U R N I N E


    Acknowledgements:
    - Thanks Dano for faceplate / 3D models & project inspiration!

*/

// include the library code:
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <WiFiManager.h>

//#include <TimeLib.h> //TimeLib library is needed https://github.com/PaulStoffregen/Time
#include <NTPClient.h>

// define how to write each of the words

// 64-bit "mask" for each pixel in the matrix- is it on or off?
uint64_t mask;

#define LANG_DE
//#define LANG_EN

#ifdef LANG_EN
#define MFIVE mask |= 0xF00000000000        // these are in hexadecimal
#define MTEN mask |= 0x5800000000000000
#define AQUARTER mask |= 0x80FE000000000000
#define TWENTY mask |= 0x7E00000000000000
#define HALF mask |= 0xF0000000000
#define PAST mask |= 0x7800000000
#define TO mask |= 0xC00000000
#define ONE mask |= 0x43
#define TWO mask |= 0xC040
#define THREE mask |= 0x1F0000
#define FOUR mask |= 0xF0
#define FIVE mask |= 0xF0000000
#define SIX mask |= 0xE00000
#define SEVEN mask |= 0x800F00
#define EIGHT mask |= 0x1F000000
#define NINE mask |= 0xF
#define TEN mask |= 0x1010100
#define ELEVEN mask |= 0x3F00
#define TWELVE mask |= 0xF600
#define ANDYDORO mask |= 0x8901008700000000
#endif

#ifdef LANG_DE
#define MFIVE mask    |= 0xF000000000000000        // these are in hexadecimal
#define MTEN mask     |= 0x0F00000000000000
#define AQUARTER mask |= 0xFF00000000000000
#define TWENTY mask   |= 0x7E00000000000000 // was disabled
#define HALF mask     |= 0x0000F00000000000
#define PAST mask     |= 0x001E000000000000
#define TO mask       |= 0x00E0000000000000
#define ONE mask      |= 0x000000F000000000
#define TWO mask   |= 0x18060000
#define THREE mask |= 0x001E0000
#define FOUR mask     |= 0x00000F0000000000
#define FIVE mask  |= 0x01010101
#define SIX mask      |= 0x0000001F00000000
#define SEVEN mask |= 0xE0E00000
#define EIGHT mask |= 0x000000F0
#define NINE mask  |= 0x00000F00
#define TEN mask   |= 0x00007800
#define ELEVEN mask |= 0x00000007
#define TWELVE mask |= 0x1F000000
#define ANDYDORO mask |= 0x89010087  // was disabled
#endif

// define pins
#define NEOPIN 3  // connect to DIN on NeoMatrix 8x8
//#define RTCGND 12//A2 // use this as DS1307 breakout ground 
//#define RTCPWR 13//A3 // use this as DS1307 breakout power


// brightness based on time of day- could try warmer colors at night?
#define DAYBRIGHTNESS 80
#define NIGHTBRIGHTNESS 20

// cutoff times for day / night brightness. feel free to modify.
#define MORNINGCUTOFF 7  // when does daybrightness begin?   7am
#define NIGHTCUTOFF   22 // when does nightbrightness begin? 10pm


// define delays
#define FLASHDELAY 250  // delay for startup "flashWords" sequence
#define SHIFTDELAY 100   // controls color shifting speed

//RTC_DS1307 RTC; // Establish clock object
//DST_RTC dst_rtc; // DST object

// Define US or EU rules for DST comment out as required. More countries could be added with different rules in DST_RTC.cpp
//const char rulesDST[] = "US"; // US DST rules
// const char rulesDST[] = "EU";   // EU DST rules

//DateTime theTime; // Holds current clock time

int j;   // an integer for the color shifting effect

// Do you live in a country or territory that observes Daylight Saving Time?
// https://en.wikipedia.org/wiki/Daylight_saving_time_by_country
// Use 1 if you observe DST, 0 if you don't. This is programmed for DST in the US / Canada. If your territory's DST operates differently,
// you'll need to modify the code in the calcTheTime() function to make this work properly.
//#define OBSERVE_DST 1


// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//Adafruit_NeoPixel matrix = Adafruit_NeoPixel(64, NEOPIN, NEO_GRB + NEO_KHZ800);

// configure for 8x8 neopixel matrix
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, NEOPIN,
                            NEO_MATRIX_TOP  + NEO_MATRIX_LEFT +
                            NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
                            NEO_GRB         + NEO_KHZ800);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);

void setup() {
  // put your setup code here, to run once:

  //Serial for debugging
  Serial.begin(115200);
  Serial.println("\nsetup");

  // set pinmodes
  pinMode(NEOPIN, OUTPUT);

  matrix.begin();
  matrix.setBrightness(DAYBRIGHTNESS);
  matrix.fillScreen(0); // Initialize all pixels to 'off'
  matrix.show();

  // startup sequence... do colorwipe?
  // delay(500);
  // rainbowCycle(20);
  delay(500);
  //flashWords(); // briefly flash each word in sequence
  //delay(500);

  WiFiManager wifiManager;
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  wifiManager.setTimeout(180);
  if (!wifiManager.autoConnect("Klausi UDP Status")) {
    Serial.println("failed to connect and hit timeout. resetting ...");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }
  timeClient.begin();
}

void displayTime(int hours, int minutes);
void applyMask();
uint32_t Wheel(byte WheelPos);
int minutes;
void loop() {
  // put your main code here, to run repeatedly:

  //theTime = dst_rtc.calculateTime(RTC.now()); // takes into account DST
  // add 2.5 minutes to get better estimates
  //theTime = theTime.unixtime() + 150;
  //adjustBrightness(timeClient.getHours());
#if 1
  // get the time
  timeClient.update();
  Serial.println(timeClient.getFormattedTime());
  displayTime(timeClient.getHours(), timeClient.getMinutes());
#else
  displayTime((minutes/60)%24, minutes%60);
  minutes=minutes+5;
#endif
  delay (1000);
}
