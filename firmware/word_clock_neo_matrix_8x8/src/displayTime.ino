
// function to generate the right "phrase" based on the time

void displayTime(int hours, int minutes) {

	// time we display the appropriate minutes counter
  	if ((minutes > 4) && (minutes < 10)) {
		MFIVE;
		PAST;
  	}
  
	if ((minutes > 9) && (minutes < 15)) {
		MTEN;
		PAST;
	}
  
  	if ((minutes > 14) && (minutes < 20)) {
		AQUARTER;
		PAST;
  	}
  
  	if ((minutes > 19) && (minutes < 25)) {
		MTEN;
		TO;
		HALF;
  	}
  
  	if ((minutes > 24) && (minutes < 30)) {
		MFIVE;
		TO;
		HALF;
  	}
  	
  	if ((minutes > 29) && (minutes < 35))
		HALF;
  
  	if ((minutes > 34) && (minutes < 40)) {
		MFIVE;
		PAST;
		HALF;
  	}
  
  	if ((minutes > 39) && (minutes < 45)) {
		MTEN;
		PAST;
		HALF;
  	} 
  
  	if ((minutes > 44) && (minutes < 50)) {
		AQUARTER;
		TO;
  	}
  
  	if ((minutes > 49) && (minutes < 55)) {
		MTEN;
		TO;
  	}
  	
  	if (minutes > 54) {
		MFIVE;
		TO;
  	}
  
	if ((minutes < 20))
  	{
		switch (hours) {
      		case 1:
      		case 13:
        		ONE;
        		break;
        		
      		case 2:
      		case 14:
        		TWO;
        		break;
        		
		case 3:
		case 15:
		        THREE;
		        break;
		        
		case 4:
		case 16:
		        FOUR;
		        break;
		        
		case 5:
		case 17:
		        FIVE;
		        break;
		        
		case 6:
		case 18:
		        SIX;
		        break;
		        
		case 7:
		case 19:
		        SEVEN;
		        break;
		        
		case 8:
		case 20:
		        EIGHT;
		        break;
		        
		case 9:
		case 21:
		        NINE;
		        break;
		        
		case 10:
		case 22:
		        TEN;
		        break;
		        
		case 11:
		case 23:
		        ELEVEN;
		        break;
		        
		case 0:
		case 12:
		        TWELVE;
		        break;
		}
	}
  	else
  	{
   
		switch (hours) {
		case 1:
		case 13:
		        TWO;
		        break;
		        
		case 14:
		case 2:
		        THREE;
		        break;
		        
		case 15:
		case 3:
		        FOUR;
		        break;
		        
		case 4:
		case 16:
		        FIVE;
		        break;
		        
		case 5:
		case 17:
		        SIX;
		        break;
		        
		case 6:
		case 18:
		        SEVEN;
		        break;
		        
		case 7:
		case 19:
		        EIGHT;
		        break;
		        
		case 8:
		case 20:
		        NINE;
		        break;
		        
		case 9:
		case 21:
		        TEN;
		        break;
		        
		case 10:
		case 22:
		        ELEVEN;
		        break;
		        
		case 11:
		case 23:
		        TWELVE;
		        break;
		        
		case 0:
		case 12:
		        ONE;
		        break;
		}
  	}
  	
  	applyMask(); // apply phrase mask to colorshift function
}
