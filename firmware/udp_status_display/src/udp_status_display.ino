#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <WiFiManager.h>

#define PIN D6
#define NUM_LEDS 10

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);
WiFiClient net;
WiFiUDP Udp;
unsigned int localUdpPort = 5555;
char incomingPacket[255];

void setup() {
  Serial.begin(115200);

  strip.begin();
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }
  strip.show();

  WiFiManager wifiManager;
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  wifiManager.setTimeout(180);
  if (!wifiManager.autoConnect("Klausi UDP Status")) {
    Serial.println("failed to connect and hit timeout. resetting ...");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  Udp.begin(localUdpPort);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);

  initSequence();
}

void initSequence() {

  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(255, 0, 0));
    strip.show();
    delay(50);
  }
for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(0, 255, 0));
    strip.show();
    delay(50);
  }
for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 255));
    strip.show();
    delay(50);
  }

  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
    strip.show();
    delay(50);
  }
}

void loop() {
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0) {
      incomingPacket[len] = 0;
    }

    String sPacket = String(incomingPacket);
    String sValue = sPacket.substring(sPacket.lastIndexOf(":") + 1);
    int ledIndex = sPacket.substring(0, sPacket.lastIndexOf(":")).toInt() - 1;

    if (ledIndex >= 0 && ledIndex < strip.numPixels()) {
      if (sValue == "off") {
        strip.setPixelColor(ledIndex, strip.Color(0, 0, 0));
      } if (sValue == "init") {
        initSequence();
      } else {
        long number = (long)strtol(&sValue[0], NULL, 16);
        int r = number >> 16;
        int g = number >> 8 & 0xFF;
        int b = number & 0xFF;

        strip.setPixelColor(ledIndex, strip.Color(r, g, b));
      } 

      strip.show();
    }

    Serial.printf("UDP packet contents: %s\n", incomingPacket);

    // Antwort
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write("OK");
    Udp.endPacket();
  }
}